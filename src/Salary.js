import React from 'react';
import { Table, Grid, Row, Col, ButtonToolbar, ButtonGroup, Button, Glyphicon } from 'react-bootstrap';

export default class Salary extends React.Component{
  constructor(props){
    super(props);
    this.state = {

    }
  }

  render(){
    return(
      <div>
        <Grid>
        <Row className="show-grid">
        <Col xs={10} xsOffset={1}>

          <Table striped bordered condensed hover>
            <thead>
              <tr>
              <th>#</th>
                <th>Employee Code</th>
                <th>Employee Name</th>
                <th>Designation</th>
                <th>Gross Salary</th>
                <th colSpan="3" className="text-center">Action</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>1</td>
                <td>1001</td>
                <td>Mark</td>
                <td>Manager</td>
                <td>25000</td>
                <td>
                  <ButtonToolbar>
                    <ButtonGroup>
                      <Button bsSize="small" className="btn-warning">
                        <Glyphicon glyph="edit" />
                      </Button>
                    </ButtonGroup>
                  </ButtonToolbar>
                </td>
                <td>
                  <ButtonToolbar>
                    <ButtonGroup>
                      <Button bsSize="small" className="btn-info">
                        <Glyphicon glyph="eye-open" />
                      </Button>
                    </ButtonGroup>
                  </ButtonToolbar>
                </td>
                <td>
                  <ButtonToolbar>
                    <ButtonGroup>
                      <Button bsSize="small" className="btn-danger">
                        <Glyphicon glyph="remove" />
                      </Button>
                    </ButtonGroup>
                  </ButtonToolbar>
                </td>
              </tr>
              <tr>
                <td>2</td>
                <td>1002</td>
                <td>Jacob</td>
                <td>DGM</td>
                <td>35000</td>
                <td>
                  <ButtonToolbar>
                    <ButtonGroup>
                      <Button bsSize="small" className="btn-warning">
                        <Glyphicon glyph="edit" />
                      </Button>
                    </ButtonGroup>
                  </ButtonToolbar>
                </td>
                <td>
                  <ButtonToolbar>
                    <ButtonGroup>
                      <Button bsSize="small" className="btn-info">
                        <Glyphicon glyph="eye-open" />
                      </Button>
                    </ButtonGroup>
                  </ButtonToolbar>
                </td>
                <td>
                  <ButtonToolbar>
                    <ButtonGroup>
                      <Button bsSize="small" className="btn-danger">
                        <Glyphicon glyph="remove" />
                      </Button>
                    </ButtonGroup>
                  </ButtonToolbar>
                </td>
              </tr>
              <tr>
                <td>3</td>
                <td>1003</td>
                <td>Jacob</td>
                <td>DGM</td>
                <td>35000</td>
                <td>
                  <ButtonToolbar>
                    <ButtonGroup>
                      <Button bsSize="small" className="btn-warning">
                        <Glyphicon glyph="edit" />
                      </Button>
                    </ButtonGroup>
                  </ButtonToolbar>
                </td>
                <td>
                  <ButtonToolbar>
                    <ButtonGroup>
                      <Button bsSize="small" className="btn-info">
                        <Glyphicon glyph="eye-open" />
                      </Button>
                    </ButtonGroup>
                  </ButtonToolbar>
                </td>
                <td>
                  <ButtonToolbar>
                    <ButtonGroup>
                      <Button bsSize="small" className="btn-danger">
                        <Glyphicon glyph="remove" />
                      </Button>
                    </ButtonGroup>
                  </ButtonToolbar>
                </td>
              </tr>
              <tr>
                <td>4</td>
                <td>1004</td>
                <td>Jacob</td>
                <td>DGM</td>
                <td>35000</td>
                <td>
                  <ButtonToolbar>
                    <ButtonGroup>
                      <Button bsSize="small" className="btn-warning">
                        <Glyphicon glyph="edit" />
                      </Button>
                    </ButtonGroup>
                  </ButtonToolbar>
                </td>
                <td>
                  <ButtonToolbar>
                    <ButtonGroup>
                      <Button bsSize="small" className="btn-info">
                        <Glyphicon glyph="eye-open" />
                      </Button>
                    </ButtonGroup>
                  </ButtonToolbar>
                </td>
                <td>
                  <ButtonToolbar>
                    <ButtonGroup>
                      <Button bsSize="small" className="btn-danger">
                        <Glyphicon glyph="remove" />
                      </Button>
                    </ButtonGroup>
                  </ButtonToolbar>
                </td>
              </tr>
            </tbody>
        </Table>
        </Col>
        </Row>
        </Grid>

      </div>
    )
  }
}
