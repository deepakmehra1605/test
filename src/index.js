import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Main from './Main';
import registerServiceWorker from './registerServiceWorker';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';

ReactDOM.render(
  <Router>
    <div>
      <Route path="/" component={Main} />

    </div>
  </Router>, document.getElementById('root'));
registerServiceWorker();
