import React from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import { Navbar, Nav, NavItem, NavDropdown, MenuItem} from 'react-bootstrap';

export default class Header extends React.Component{
  constructor(props){
    super(props);
    this.state = {

    }
  }



  render(){
    return(
      <React.Fragment>
          <Navbar inverse>
            <Navbar.Header>
              <Navbar.Brand>
                <a href="/">HRM</a>
              </Navbar.Brand>
            </Navbar.Header>
            <Nav>
            <NavItem eventKey={1} href="/">Home</NavItem>
            <NavDropdown eventKey={2} title="Employees" id="basic-nav-dropdown">
              <MenuItem eventKey={2.1} href="/View">View</MenuItem>
              <MenuItem eventKey={2.2}>Another action</MenuItem>
              <MenuItem eventKey={2.3}>Something else here</MenuItem>
              <MenuItem divider />
              <MenuItem eventKey={2.4}>Separated link</MenuItem>
            </NavDropdown>
            <NavDropdown eventKey={3} title="Letters" id="basic-nav-dropdown">
              <MenuItem eventKey={3.1} href="/Letters">Letters</MenuItem>
              <MenuItem eventKey={3.2}>Another action</MenuItem>
              <MenuItem eventKey={3.3}>Something else here</MenuItem>
              <MenuItem divider />
              <MenuItem eventKey={3.4}>Separated link</MenuItem>
            </NavDropdown>
            <NavDropdown eventKey={4} title="Salary" id="basic-nav-dropdown">
              <MenuItem eventKey={4.1} href="/Salary">Salary</MenuItem>
              <MenuItem eventKey={4.2}>Another action</MenuItem>
              <MenuItem eventKey={4.3}>Something else here</MenuItem>
              <MenuItem divider />
              <MenuItem eventKey={4.4}>Separated link</MenuItem>
            </NavDropdown>
            <NavDropdown eventKey={5} title="Attendance" id="basic-nav-dropdown">
              <MenuItem eventKey={5.1} href="/Attendance">Attendance</MenuItem>
              <MenuItem eventKey={5.2} href="/Leaves">Leaves</MenuItem>
              <MenuItem eventKey={5.3} href="/Holidays">Holidays</MenuItem>
              <MenuItem divider />
              <MenuItem eventKey={5.4}>Separated link</MenuItem>
            </NavDropdown>

            </Nav>
            <Nav pullRight>
            <NavItem eventKey={6} href="/Login">Login</NavItem>
            </Nav>
          </Navbar>

      </React.Fragment>
    )
  }
}
