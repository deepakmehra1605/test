import React from 'react';
import './App.css';

export default class App extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      month : 'January 2018',
      name : 'Deepak Kumar',
      designation : 'HR',
      ecode : 'TS-1004',
      department : 'Human Resource',
      doj : '01-09-2016',
      pan : 'BHVPA5447G',
      pfNo : '--',
      account : '001591800049800',
      working :28,
      leaves : 0,
      pending : 1.5,
      home : 0,
      absent : 0,
      basic : 10000,
      hra : 4500,
      conveyance : 3500,
      academic : 2500,
      uniform : 2500,
      medical : 2000,
      other : 1000,
      bonus : 1000,
      provident : 0,
      esi : 0,
      security : 0,
      tds : 0,
      pf: false,
      es: false,
      sec: false
    }
  }

  onChange = (key, e) => {
      const {basic, hra, conveyance, academic, uniform, medical, other, bonus, provident, esi, security,tds,working, absent} = this.state
      let worked = working - absent
      let calculate = worked / working
      let gross = (basic + hra + conveyance + academic + uniform + medical + other + bonus)
      if(key === 'pf') {
        this.setState({ provident: e.target.checked?((this.state.basic*0.12)*calculate):0 });
      } else if(key === 'es') {console.log(gross)
        this.setState({ esi: e.target.checked?(gross*0.0175)*calculate:0});
      } else if(key === 'sec') {
        this.setState({ security: e.target.checked?gross*0.1:0 });
      }
  }

  render(){
    const{basic, hra, conveyance, academic, uniform, medical, other, bonus, provident, esi, security,tds,working, absent} = this.state
    let worked = working - absent
    let calculate = worked / working
    let gross = (basic + hra + conveyance + academic + uniform + medical + other + bonus) * calculate
    let deduction = ((provident + esi) * calculate) + security + tds
    let net = gross - deduction

    return(
      <React.Fragment>

      <header className="clearfix">
        <div id="logo">
          <img src={require('./assets/images/logo.png')} />
        </div>
        <div id="company">
          <h2 className="name">Company Name</h2>
          <div>455 Foggy Heights, AZ 85004, US</div>
          <div>(602) 519-0450</div>
          <div><a href="mailto:company@example.com">company@example.com</a></div>
        </div>
      </header>
      <main>
        <div id="details" className="clearfix">
          <div style={{ textAlign: 'center'}}>
            <h1>Salary Slip </h1><h2> {this.state.month}</h2>
          </div>
        </div>
        <table border="0" cellspacing="0" cellpadding="0">
          <thead>
            <tr>
              <th className="no">Name</th>
              <th className="desc">{this.state.name}</th>
              <th className="no">Account No.</th>
              <th className="desc">{this.state.account}</th>

            </tr>
          </thead>
          <tbody>
            <tr>
              <td className="no">Designation</td>
              <td className="desc">{this.state.designation}</td>
              <td className="no">No. of Working Days</td>
              <td className="desc">{this.state.working}</td>

            </tr>
            <tr>
              <td className="no">Employee Code</td>
              <td className="desc">{this.state.ecode}</td>
              <td className="no">Day Worked</td>
              <td className="desc">{worked}</td>

            </tr>
            <tr>
              <td className="no">Department</td>
              <td className="desc">{this.state.department}</td>
              <td className="no">Leaves Availed</td>
              <td className="desc">{this.state.leaves}</td>

            </tr>
            <tr>
              <td className="no">Date of Joining</td>
              <td className="desc">{this.state.doj}</td>
              <td className="no">Leaves Pending</td>
              <td className="desc">{this.state.pending}</td>

            </tr>
            <tr>
              <td className="no">PAN</td>
              <td className="desc">{this.state.pan}</td>
              <td className="no">Work from Home</td>
              <td className="desc">{this.state.home}</td>

            </tr>
            <tr>
              <td className="no">PF No.</td>
              <td className="desc">{this.state.pfNo}</td>
              <td className="no">Absent</td>
              <td className="desc">{this.state.absent}</td>

            </tr>
          </tbody>
          <thead>
            <tr>
              <th className="no">EARNINGS</th>
              <th className="no">RS.</th>
              <th className="no">DEDUCTIONS</th>
              <th className="no">RS.</th>

            </tr>
          </thead>
          <tbody>
            <tr>
              <td className="no">Basic Salary</td>
              <td className="desc"><h3>{parseFloat(basic * calculate).toFixed(2)}</h3> </td>
              <td className="no">Provident Fund<input className="check" type="checkbox" onChange={this.onChange.bind(this, 'pf')} /></td>
              <td className="desc"><h3>{parseFloat(provident).toFixed(2)}</h3></td>

            </tr>
            <tr>
              <td className="no">House Rent Allowance</td>
              <td className="desc"><h3>{parseFloat(hra * calculate).toFixed(2)}</h3> </td>
              <td className="no">ESI (1.75%)<input className="check" type="checkbox" onChange={this.onChange.bind(this, 'es')} /></td>
              <td className="desc"><h3>{parseFloat(esi).toFixed(2)}</h3></td>

            </tr>
            <tr>
              <td className="no">Conveyance Allowance</td>
              <td className="desc"><h3>{parseFloat(conveyance * calculate).toFixed(2)}</h3> </td>
              <td className="no">Security Amount<input className="check" type="checkbox" onChange={this.onChange.bind(this, 'sec')} /></td>
              <td className="desc"><h3>{parseFloat(security).toFixed(2)}</h3></td>

            </tr>
            <tr>
              <td className="no">Academic Allowance</td>
              <td className="desc"><h3>{parseFloat(academic * calculate).toFixed(2)}</h3> </td>
              <td className="no">TDS</td>
              <td className="desc"><h3>{parseFloat(tds).toFixed(2)}</h3></td>

            </tr>
            <tr>
              <td className="no">Uniform Allowance</td>
              <td className="desc"><h3>{parseFloat(uniform * calculate).toFixed(2)}</h3> </td>
              <td className="no"></td>
              <td className="desc"><h3></h3></td>

            </tr>
            <tr>
              <td className="no">Medical Allowance</td>
              <td className="desc"><h3>{parseFloat(medical).toFixed(2)}</h3> </td>
              <td className="no"></td>
              <td className="desc"><h3></h3></td>

            </tr>
            <tr>
              <td className="no">Others</td>
              <td className="desc"><h3>{parseFloat(other * calculate).toFixed(2)}</h3> </td>
              <td className="no"></td>
              <td className="desc"><h3></h3></td>

            </tr>
            <tr>
              <td className="no">Bonus</td>
              <td className="desc"><h3>{parseFloat(bonus).toFixed(2)}</h3> </td>
              <td className="no"></td>
              <td className="desc"><h3></h3></td>

            </tr>
            <tr>
              <td className="no">Total Earnings</td>
              <td className="desc"><h3>{parseFloat(gross).toFixed(2)}</h3> </td>
              <td className="no">Total Deductions</td>
              <td className="desc"><h3>{parseFloat(deduction).toFixed(2)}</h3></td>

            </tr>
            <tr>
              <td className="no"></td>
              <td className="desc"><h3></h3> </td>
              <td className="no">Net Pay</td>
              <td className="desc"><h3>{parseFloat(net).toFixed(2)}</h3></td>

            </tr>
          </tbody>

        </table>

        <div id="notices">
          <div className="notice1">For the company</div>
          <div className="notice">Authorized Signatory</div>
        </div>
      </main>
      <footer>
        Salary Slip was created on a computer and is not valid without the signature and seal.
      </footer>


      </React.Fragment>
    )
  }
}
