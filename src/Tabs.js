import React from 'react';


export default class Footer extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      tab: 1
    }
  }
  renderTab = () => {
    switch(this.state.tab){
      case 1: return <p>Tab1</p>
      case 2: return <p>Tab2</p>
      case 3: return <p>Tab3</p>
    }
  }

  render(){
    return(
      <React.Fragment>


  <div>
    <div>
    <button onClick={() => this.setState({ tab: 1 })} className={this.state.tab === 1?'active':null}>Tab1</button>
    <button onClick={() => this.setState({ tab: 2 })} className={this.state.tab === 2?'active':null}>Tab2</button>
    <button onClick={() => this.setState({ tab: 3 })} className={this.state.tab === 3?'active':null}>Tab3</button>
    </div>
    <div>
        {this.renderTab()}
    </div>
  </div>
      </React.Fragment>
    )
  }
}
