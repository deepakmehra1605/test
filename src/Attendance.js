import React from 'react';
import { Table, Grid, Row, Col } from 'react-bootstrap';

export default class Attendance extends React.Component{
  constructor(props){
    super(props);
    this.state = {

    }
  }

  render(){
    return(
      <div>
        <Grid>
        <Row className="show-grid">
        <Col xs={10} xsOffset={1}>
        <Table responsive>
  <thead>
    <tr>
      <th>Employee Code</th>
      <th>Employee Name</th>
      <th>Month & Year</th>
      <th>No of Days</th>
      <th>Working Days</th>
      <th>Absent</th>
      <th>Leaves</th>
      <th>Work From Home</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>1001</td>
      <td>Deepak</td>
      <td>April-2018</td>
      <td>30</td>
      <td>30</td>
      <td>00</td>
      <td>01</td>
      <td>00</td>
    </tr>
    <tr>
      <td>1001</td>
      <td>Deepak</td>
      <td>April-2018</td>
      <td>30</td>
      <td>30</td>
      <td>00</td>
      <td>01</td>
      <td>00</td>
    </tr><tr>
      <td>1001</td>
      <td>Deepak</td>
      <td>April-2018</td>
      <td>30</td>
      <td>30</td>
      <td>00</td>
      <td>01</td>
      <td>00</td>
    </tr><tr>
      <td>1001</td>
      <td>Deepak</td>
      <td>April-2018</td>
      <td>30</td>
      <td>30</td>
      <td>00</td>
      <td>01</td>
      <td>00</td>
    </tr><tr>
      <td>1001</td>
      <td>Deepak</td>
      <td>April-2018</td>
      <td>30</td>
      <td>30</td>
      <td>00</td>
      <td>01</td>
      <td>00</td>
    </tr><tr>
      <td>1001</td>
      <td>Deepak</td>
      <td>April-2018</td>
      <td>30</td>
      <td>30</td>
      <td>00</td>
      <td>01</td>
      <td>00</td>
    </tr><tr>
      <td>1001</td>
      <td>Deepak</td>
      <td>April-2018</td>
      <td>30</td>
      <td>30</td>
      <td>00</td>
      <td>01</td>
      <td>00</td>
    </tr><tr>
      <td>1001</td>
      <td>Deepak</td>
      <td>April-2018</td>
      <td>30</td>
      <td>30</td>
      <td>00</td>
      <td>01</td>
      <td>00</td>
    </tr><tr>
      <td>1001</td>
      <td>Deepak</td>
      <td>April-2018</td>
      <td>30</td>
      <td>30</td>
      <td>00</td>
      <td>01</td>
      <td>00</td>
    </tr><tr>
      <td>1001</td>
      <td>Deepak</td>
      <td>April-2018</td>
      <td>30</td>
      <td>30</td>
      <td>00</td>
      <td>01</td>
      <td>00</td>
    </tr><tr>
      <td>1001</td>
      <td>Deepak</td>
      <td>April-2018</td>
      <td>30</td>
      <td>30</td>
      <td>00</td>
      <td>01</td>
      <td>00</td>
    </tr><tr>
      <td>1001</td>
      <td>Deepak</td>
      <td>April-2018</td>
      <td>30</td>
      <td>30</td>
      <td>00</td>
      <td>01</td>
      <td>00</td>
    </tr><tr>
      <td>1001</td>
      <td>Deepak</td>
      <td>April-2018</td>
      <td>30</td>
      <td>30</td>
      <td>00</td>
      <td>01</td>
      <td>00</td>
    </tr>
  </tbody>
</Table>
</Col>
</Row>
</Grid>
      </div>
    )
  }
}
