import React from 'react';
import { Form, FormGroup, ControlLabel, FormControl, Col, Checkbox, Button, Grid, Row, Panel } from 'react-bootstrap';
import './assets/css/header.css'

export default class Login extends React.Component{
  constructor(props){
    super(props);
    this.state = {

    }
  }

  render(){
    return(
      <div className="body">
      <Grid>
      <Row className="show-grid">
      <Col xs={5} xsOffset={3}>
          <Panel bsStyle="info">
            <Panel.Heading>
              <Panel.Title componentClass="h3">Login Form</Panel.Title>
            </Panel.Heading>
            <Panel.Body>
            <Form horizontal>
                <FormGroup controlId="formHorizontalEmail">
                  <Col componentClass={ControlLabel} sm={3}>
                    Email
                  </Col>
                  <Col sm={7}>
                    <FormControl type="email" placeholder="Email" />
                  </Col>
                </FormGroup>

                <FormGroup controlId="formHorizontalPassword">
                  <Col componentClass={ControlLabel} sm={3}>
                    Password
                  </Col>
                  <Col sm={7}>
                    <FormControl type="password" placeholder="Password" />
                  </Col>
                </FormGroup>

                <FormGroup>
                  <Col smOffset={3} sm={6}>
                    <Checkbox>Remember me</Checkbox>
                  </Col>
                </FormGroup>

                <FormGroup>
                  <Col smOffset={3} sm={6}>
                    <Button className="btn btn-primary" type="submit">Sign in</Button>
                  </Col>
                </FormGroup>
              </Form>
            </Panel.Body>
          </Panel>

      </Col>
      </Row>



      </Grid>
      </div>
    )
  }
}
