import React, {Component} from 'react';
import Header from './Header';
import Footer from './Footer';
import View from './View';
import Letters from './Letters';
import Attendance from './Attendance';
import Salary from './Salary';
import Login from './Login';
import Leaves from './Leaves';
import Holidays from './Holidays';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';

export default class Main extends Component{
  render(){
    return(
      <div>
      <Header />
      <Route path="/View" component={View} />
      <Route path="/Letters" component={Letters} />
      <Route path="/Salary" component={Salary} />
      <Route path="/Attendance" component={Attendance} />
      <Route path="/Login" component={Login} />
      <Route path="/Leaves" component={Leaves} />
      <Route path="/Holidays" component={Holidays} />
      </div>
    )
  }
}
