import React from 'react';
import './index.css';

export default class Toggle extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      show: true
    }
  }

  toggle = () => {

    this.setState({show: !this.state.show})
  }
  render(){
    return(
      <React.Fragment>
      {this.state.show?<div>Hello World</div>:null}
<button onClick={this.toggle}>{this.state.show?'Hide':'Show'}</button>


      </React.Fragment>
    )
  }
}
