import React from 'react';
import { Table, Grid, Row, Col, ButtonToolbar, ButtonGroup, Button, Glyphicon, Modal, Form, FormGroup, ControlLabel, FormControl } from 'react-bootstrap';

export default class Add extends React.Component{
  constructor(props, context){
    super(props, context);

    this.state = {
      employee: {
        empCode: '',
        name: '',
        designation: '',
        salary: '',
        dob: '',
        doj: '',
        contact: ''
      },

      editable: false,
      view: false,
      key:'',
      employees: [
        {
           empCode: 1001,
           name: 'Mark',
           designation: 'Manager',
           salary: 25000,
           dob: '16-05-1987',
           doj: '16-05-2017',
           contact: 1234567890
        },
        {
           empCode: 1002,
           name: 'Jacob',
           designation: 'DGM',
           salary: 35000,
           dob: '16-05-1987',
           doj: '16-05-2017',
           contact: 1234567890
        }

      ],
        show: false
    }
  }

  onChange = (key, e) => {
    let { employee } = this.state;
    employee[key] = e.target.value;
    this.setState({employee});
  }

  onSubmit = (e) => {
    e.preventDefault();
    let { employee, employees, key, editable} = this.state;
    if (employee.name){
      if(editable) {
        employees[key] = employee;
      } else {
        employees.push(employee);
      }
      employee = {};
      this.setState({employees, employee});
      this.handleHide();
    }
  }

  handleHide = () => {


    this.setState({ show: false, editable: false, view: false });
  }

  onDelete = (key) => {
    let { employees } = this.state;
    employees.splice(key, 1);

    this.setState({ employees });
  }

  onEdit = (key, employee) => {
    this.setState({editable: true, employee, key, show: true});
  }

  onView = (employee) => {
    this.setState({ employee, view: true, show: true });
  }

  onAdd = () => {
    this.setState({ show: true, employee: {} });
  }

  render(){ console.log(this.state.employees)
    return(
      <div>

        <Grid>
        <Row className="show-grid">
        <Col xs={10} xsOffset={1}>
          <Button className="pull-right" bsStyle="primary" onClick={this.onAdd}>Add Employee</Button><h2>Employees Table</h2>
          <Table striped bordered condensed hover>
            <thead>
              <tr>
              <th>#</th>
                <th>Employee Code</th>
                <th>Employee Name</th>
                <th>Designation</th>
                <th>Gross Salary</th>
                <th>Date of Birth</th>
                <th>Date of Joining</th>
                <th>Contact No</th>
                <th colSpan="3" className="text-center">Action</th>
              </tr>
            </thead>
            <tbody>
              {this.state.employees.length?this.state.employees.map((item, key) =><tr key={key}>
                <td>{key+1}</td>
                <td>{item.empCode}</td>
                <td>{item.name}</td>
                <td>{item.designation}</td>
                <td>Rs.{parseFloat(item.salary).toFixed(2)}</td>
                <td>{item.dob}</td>
                <td>{item.doj}</td>
                <td>{item.contact}</td>
                <td>
                  <ButtonToolbar>
                    <ButtonGroup>
                      <Button bsSize="small" className="btn-warning" onClick={this.onEdit.bind(this, key, item)}>
                        <Glyphicon glyph="edit" />
                      </Button>
                      <Button bsSize="small" className="btn-info" onClick={this.onView.bind(this, item)}>
                        <Glyphicon glyph="eye-open" />
                      </Button>
                      <Button bsSize="small" className="btn-danger" onClick={this.onDelete.bind(this, key)}>
                        <Glyphicon glyph="remove" />
                      </Button>
                    </ButtonGroup>
                  </ButtonToolbar>
                </td>
              </tr>):
              <tr>
                <td colSpan={9} className="text-center"><h4>No data found.</h4></td>
              </tr>
            }
            </tbody>
        </Table>
        </Col>
        </Row>
        </Grid>
        <Modal
          show={this.state.show}
          onHide={this.handleHide}
          container={this}
          aria-labelledby="contained-modal-title">
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title">
              {!this.state.editable?!this.state.view?'Add Employee':'View Employee':'Edit Employee'}
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Row className="show-grid">
              <Col xs={10} xsOffset={1}>
                <Form onSubmit={this.onSubmit}>

                    <FormGroup controlId="formInlineName">
                    <Row className="show-grid">
                    <Col xs={6}>
                      <ControlLabel>Employee Code:</ControlLabel>
                    </Col>
                      {this.state.view?<Col xs={6}>{this.state.employee.empCode}</Col>:<Col xs={6}><FormControl type="text" placeholder="Employee Code" onChange={this.onChange.bind(this, 'empCode')} value={this.state.employee.empCode} /></Col>}
                      </Row>
                    </FormGroup>


                    <FormGroup controlId="formInlineName">
                    <Row className="show-grid">
                      <Col xs={6}>
                        <ControlLabel>Employee Name:</ControlLabel>
                      </Col>
                        {this.state.view?<Col xs={6}>{this.state.employee.name}</Col>:<Col xs={6}><FormControl type="text" placeholder="Employee Name" onChange={this.onChange.bind(this, 'name')} value={this.state.employee.name} /></Col>}
                    </Row>
                    </FormGroup>


                    <FormGroup ControlId="formInlineName">
                    <Row className="show-grid">
                      <Col xs={6}>
                        <ControlLabel>Designation:</ControlLabel>
                      </Col>
                        {this.state.view?<Col xs={6} md={6}>{this.state.employee.designation}</Col>:<Col xs={6}><FormControl type="text" placeholder="Designation" onChange={this.onChange.bind(this, 'designation')} value={this.state.employee.designation} /></Col>}
                    </Row>
                    </FormGroup>

                    <FormGroup ControlId="formInlineName">
                    <Row className="show-grid">
                      <Col xs={6}>
                        <ControlLabel>Gross Salary:</ControlLabel>
                      </Col>
                        {this.state.view?<Col xs={6}>{this.state.employee.salary}</Col>:<Col xs={6}><FormControl type="text" placeholder="Gross Salary" onChange={this.onChange.bind(this, 'salary')} value={this.state.employee.salary} /></Col>}
                    </Row>
                    </FormGroup>

                    <FormGroup ControlId="formInlineName">
                    <Row className="show-grid">
                      <Col xs={6}>
                        <ControlLabel>Date Of Birth:</ControlLabel>
                      </Col>
                        {this.state.view?<Col xs={6}>{this.state.employee.dob}</Col>:<Col xs={6}><FormControl type="text" placeholder="Date of Birth" onChange={this.onChange.bind(this, 'dob')} value={this.state.employee.dob} /></Col>}
                    </Row>
                    </FormGroup>

                    <FormGroup ControlId="formInlineName">
                    <Row className="show-grid">
                      <Col xs={6}>
                        <ControlLabel>Date Of Joining:</ControlLabel>
                      </Col>
                        {this.state.view?<Col xs={6}>{this.state.employee.doj}</Col>:<Col xs={6}><FormControl type="text" placeholder="Date of Joining" onChange={this.onChange.bind(this, 'doj')} value={this.state.employee.doj} /></Col>}
                    </Row>
                    </FormGroup>

                    <FormGroup ControlId="formInlineName">
                    <Row className="show-grid">
                      <Col xs={6}>
                        <ControlLabel>Contact No:</ControlLabel>
                      </Col>
                        {this.state.view?<Col xs={6}>{this.state.employee.contact}</Col>:<Col xs={6}><FormControl type="text" placeholder="Contact No" onChange={this.onChange.bind(this, 'contact')} value={this.state.employee.contact} /></Col>}
                    </Row>
                    </FormGroup>

                </Form>
              </Col>
            </Row>
          </Modal.Body>
          <Modal.Footer>

          {this.state.view?<Button type="submit" className="btn-primary pull-right" onClick={this.handleHide}>OK</Button>:<Button type="submit" className="btn-primary pull-right" onClick={this.onSubmit}>Submit</Button>}

          </Modal.Footer>
        </Modal>
      </div>
    )
  }
}
